# 1.  Generate of class --------------------------------------------------------


GeneratePerClass <- 
  function(par) {
       
    # EXAMPLE     
    #
    # samples = 100, 
    # radius = c(0, 1),
    # angle = c(0, 2 * pi),
    # beta = 1, 
    # incline = 0, 
    # center = c(0, 0)
    
    theta = runif(n = par$samples,
                  min = par$angle[1], 
                  max = par$angle[2])
    
    radio = runif(n = par$samples,
                  min = par$radius[1],
                  max = par$radius[2])
    
    x = radio * cos(theta) * cos(par$incline) - 
        par$beta * radio * sin(theta) * sin(par$incline) + par$center[1]
    
    y = radio * cos(theta) * sin(par$incline) + 
        par$beta * radio * sin(theta) * cos(par$incline) + par$center[2]
    
    data = cbind(x, y)
    
    return(data)
  
}



# 1.1  Generate of Volvox [Multilabel]  ----------------------------------------


Volvox.generate <- 
  function(samples = c(1100, 100, 100, 100, 100, 100, 100)){
    
    Kc = 5/(2*sqrt(2))
    parameters = list()
    parameters[["A"]]   = list( samples = samples[1],
                                radius = c(0, 5),
                                angle = c(0, 2 * pi),
                                beta = 1, 
                                incline = 0,
                                center = c(  0,   0) )
    
    parameters[["B"]]   = list( samples = samples[2],
                                radius = c(0, 3/2),
                                angle = c(0, 2 * pi),
                                beta = 1,
                                incline = 0,
                                center = c(-Kc,  Kc) )
    
    parameters[["C"]]   = list( samples = samples[3],
                                radius = c(0, 3/2),
                                angle = c(0, 2 * pi),
                                beta = 1, 
                                incline = 0,
                                center = c( Kc,  Kc) )
    
    parameters[["D"]]   = list( samples = samples[4],
                                radius = c(0, 3/2),
                                angle = c(0, 2 * pi), 
                                beta = 1, 
                                incline = 0,
                                center = c(  0,-5/2) )
    
    parameters[["AB"]]  = list( samples = samples[5],
                                radius = c(1, 2), 
                                angle = c(0, 2 * pi),
                                beta = 1, 
                                incline = 0,
                                center = c(-Kc,  Kc) )
    
    parameters[["AC"]]  = list( samples = samples[6],
                                radius = c(1, 2),  
                                angle = c(0, 2 * pi),
                                beta = 1, 
                                incline = 0, 
                                center = c( Kc,  Kc) )
    
    parameters[["AD"]]  = list( samples = samples[7],
                                radius = c(1, 2),  
                                angle = c(0, 2 * pi),
                                beta = 1, 
                                incline = 0, 
                                center = c(0, -5/2) )
    
    data = lapply(parameters, GeneratePerClass)
    data = do.call(rbind, data)
    
    labels = cbind( matrix(data = c(1,0,0,0), ncol = samples[1], nrow = 4),
                    matrix(data = c(0,1,0,0), ncol = samples[2], nrow = 4),
                    matrix(data = c(0,0,1,0), ncol = samples[3], nrow = 4),
                    matrix(data = c(0,0,0,1), ncol = samples[4], nrow = 4),
                    matrix(data = c(1,1,0,0), ncol = samples[5], nrow = 4),
                    matrix(data = c(1,0,1,0), ncol = samples[6], nrow = 4),
                    matrix(data = c(1,0,0,1), ncol = samples[7], nrow = 4)  )
    
    
    Volvox = Multilabel(data = data, labels = labels)
    
    return(Volvox)
  
}



# 1.2  Generate of Wheel [Multilabel]  ----------------------------------------


Wheel.generate <- 
  function(samples = c(100, 100, 100, 100, 100, 100, 100)){
  
    parameters = list()
    
    parameters[["B"]]   = list( samples = samples[2],
                                radius = c(1/2, 3),  
                                angle = c(-pi/6, pi/2), 
                                beta = 1, 
                                incline = 0,
                                center = c(0, 0) )
    
    parameters[["C"]]   = list( samples = samples[3],
                                radius = c(1/2, 3),  
                                angle = c( pi/2, 7*pi/6),
                                beta = 1, 
                                incline = 0,
                                center = c(0, 0) )
    
    parameters[["D"]]   = list( samples = samples[4],
                                radius = c(1/2, 3),  
                                angle = c(7*pi/6, 11*pi/6),
                                beta = 1,
                                incline = 0, 
                                center = c(0, 0) )
    
    parameters[["AB"]]  = list( samples = samples[5],
                                radius = c(1, 2),   
                                angle = c(-pi/6, pi/2), 
                                beta = 1, 
                                incline = 0, 
                                center = c(0, 0) )
    
    parameters[["AC"]]  = list( samples = samples[6],
                                radius = c(1, 2),  
                                angle = c( pi/2, 7*pi/6),   
                                beta = 1, 
                                incline = 0, 
                                center = c(0, 0) )
    
    parameters[["AD"]]  = list( samples = samples[7],
                                radius = c(1, 2),  
                                angle = c(7*pi/6, 11*pi/6), 
                                beta = 1, 
                                incline = 0, 
                                center = c(0, 0) )
    
    parameters[["A"]]   = list( samples = samples[1],
                                radius = c(0, 3/2),
                                angle = c(0, 2*pi),
                                beta = 1,
                                incline = 0,
                                center = c(0, 0) )
    
    data = lapply(parameters, GeneratePerClass)
    data = do.call(rbind, data)
    
    labels = cbind( matrix(data = c(0,1,0,0), ncol = samples[2], nrow = 4),
                    matrix(data = c(0,0,1,0), ncol = samples[3], nrow = 4),
                    matrix(data = c(0,0,0,1), ncol = samples[4], nrow = 4),
                    matrix(data = c(1,1,0,0), ncol = samples[5], nrow = 4),
                    matrix(data = c(1,0,1,0), ncol = samples[6], nrow = 4),
                    matrix(data = c(1,0,0,1), ncol = samples[7], nrow = 4),
                    matrix(data = c(1,0,0,0), ncol = samples[1], nrow = 4)  )
    
    Wheel = Multilabel(data = data, labels = labels)
    
    return(Wheel)
  
}



# 1.3  Generic Generation [Multilabel]  ----------------------------------------


Generate <- 
  function(database, samples){
  
    expression = call(name = paste(database, "generate", sep = "."), 
                      samples = samples)
    
    return( eval(expression) )
    
}
