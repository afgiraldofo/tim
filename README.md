# TIM: Techniques for treating Imbalanced Multilabel databases #

TIM has two purposes, gathers techniques to tackle imbalance in multi-label 
learning and gives additional material to the papers:

| Ref | Title |
| ------------- | ------------- |
| [1] | [Multi-label learning by hyperparameters calibration for treating class imbalance](https://bitbucket.org/afgiraldofo/tim/wiki/HAIS2018)  |
| [2]  |   |


## References ##

[1] Giraldo-Forero, A. F., Cardona-Escobar, A. F. & Castro-Ospina, A. E.(2018). Multi-label learning by hyperparameters calibration for treating class imbalance